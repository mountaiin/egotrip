---
layout: default
---
# Ideen von mir
### (und andere Gedanken-Püpse)


Cave: Das Meiste hier ist noch [[wip]], Text folgt noch. Ich wollte es nur schon einmal aufgeschrieben haben.
- [[Bionic Reading API|Bionic Reading, aber dieses mal wirklich]] 
- [[Langsam komme ICH mir wie ein Instruct-Model vor]]
- [[Egotrip/gtd-ai]]
- [[Persönliche AI entwicklen]]
- [[Personalisierte AI entwickeln]]
- [[Bewusstsein forcieren?]]
- [[init Ziele,Träume, Wünsche, eigene Motivation]]
- [[Selbstoptimierung könnte jetzt schon beginnen]]
- [[Wir müssen dringend über bevorstehende Veränderungen reden]]
- [[Wir müssen dringend über unmittelbar bevorstehende Singularität reden]]
- [[Prompt Engineering]]