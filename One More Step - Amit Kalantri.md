---
tags: quote
---
  
When all help is stopped, when your loved ones started doubting your competence, when failure seems almost confirmed, but no matter what, if you make one more attempt, that final step will fetch you the victory.
**Amit Kalantri**

> sehr motivierend!

![](https://oaidalleapiprodscus.blob.core.windows.net/private/org-OktF1oCu6UTkdnm9anKvQw8B/user-IISs3JUtt88C6t5SHVfQ6DeF/img-2cXWOutSnqZoF4YXe4EvGhe6.png?st=2023-03-19T02%3A43%3A34Z&se=2023-03-19T04%3A43%3A34Z&sp=r&sv=2021-08-06&sr=b&rscd=inline&rsct=image/png&skoid=6aaadede-4fb3-4698-a8f6-684d7786b067&sktid=a48cca56-e6da-484e-a814-9c849652bcb3&skt=2023-03-19T03%3A28%3A46Z&ske=2023-03-20T03%3A28%3A46Z&sks=b&skv=2021-08-06&sig=lbJFJLqti3bRzR4k%2BHhmWv%2B46RkwNRjq/gG1Oq8R5/g%3D)

---
PromptInfo:
 promptId: ligAmbient
 name: 🏮
 description: Ambient Lighting Foto
config:
 append:
  bodyParams: false
  reqParams: true
 context: "prompt"
 output: '`\n![](${requestResults.data[0].url})`'
bodyParams:
 n: 1
 size: "515x512"
reqParams:
 url: "https://api.openai.com/v1/images/generations"
---

A photo of Amit Kalantri, Ambient Lighting