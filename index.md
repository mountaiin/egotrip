---
layout: default
tags: egotrip
---
# Hi, ich bin Yazan.
## Willkommen in meiner Egotrip-Zentrale
Ich denke gerne viel nach über die unendlichen Möglichkeiten des menschlichen Verstandes, über die Komplexität von neuronalen Netzen sowohl organischen als auch anorganischen Ursprungs. Ich interessiere mich außerdem noch für die kognitiven und neurobiologischen Prozesse des Lernens und der Gedächtnisbildung, für das Bewusstsein, für Bash Scripting, für Astronomie und Astrophysik, für die Etymologie mancher Wörter, für bestimmte Begrifflichkeiten überhaupt und ihre Auswirkungen, für Systeme und Dynamiken, für Schizophrenie und für Autismus, für technologische Singularität.
Ein Besuch in meinem Ego Tempel lohnt sich also.

---

[[about|Noch Mehr über Mich]]
[[Unsere "dritte Hirnhälfte"|Dritte Hirnhälfte]]
[[Egotrip/Ideen]]
[[Nice]]
[[Episches - AI pur]]
[[Poetisches - AI pur]]
[[allnotes|Alle Artikel]]